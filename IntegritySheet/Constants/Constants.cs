﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Constants
{
    public class Constants
    {
        public const string ScopeUserRead = "User.Read";

        public const string ScopeGroupMemberRead = "GroupMember.Read.All";

        public const string BearerAuthorizationScheme = "Bearer";
    }
}