using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;

namespace IntegritySheet.Controllers
{
    [Route("api/PMNotificationsGrid/{action}", Name = "PMNotificationsGridApi")]
    public class PMNotificationsGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {
            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int CoolerID;

            bool success = Int32.TryParse(queryParams["CoolerID"], out CoolerID);
            if (!success)
            {
                CoolerID = 0;
            }

            var pmnotifications = _context.PMNotifications.Select(i => new
            {
                i.ID,
                i.SISID,
                i.CoolerID,
                i.PMName,
                i.PMTime
            }).Where(x => x.CoolerID == CoolerID);

            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(pmnotifications, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new PMNotification();

            var parameterContext = form["CoolerID"];
            int CoolerID;
            bool success = Int32.TryParse(parameterContext, out CoolerID);
            if (!success)
            {
                CoolerID = 0;
            }
            model.CoolerID = CoolerID;

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            //values.Add("CoolerID", Cooler);


            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.PMNotifications.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.PMNotifications.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.PMNotifications.FirstOrDefaultAsync(item => item.ID == key);

            _context.PMNotifications.Remove(model);
            await _context.SaveChangesAsync();
        }



        private void PopulateModel(PMNotification model, IDictionary values) {
            string ID = nameof(PMNotification.ID);
            string SISID = nameof(PMNotification.SISID);
            string PMNAME = nameof(PMNotification.PMName);
            string PMTIME = nameof(PMNotification.PMTime);

            if(values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

            if(values.Contains(SISID)) {
                model.SISID = values[SISID] != null ? Convert.ToInt32(values[SISID]) : (int?)null;
            }

            if(values.Contains(PMNAME)) {
                model.PMName = Convert.ToString(values[PMNAME]);
            }

            if(values.Contains(PMTIME)) {
                model.PMTime = values[PMTIME] != null ? Convert.ToDateTime(values[PMTIME]) : (DateTime?)null;
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}