using DevExtreme.AspNet.Data;
using DevExtreme.AspNet.Mvc;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using IntegritySheet.Models;

namespace IntegritySheet.Controllers
{
    [Route("api/COCResponsesGrid/{action}", Name = "COCResponsesGridApi")]
    public class COCResponsesGridController : ApiController
    {
        private IntegritySheetEntities _context = new IntegritySheetEntities();

        [HttpGet]
        public async Task<HttpResponseMessage> Get(DataSourceLoadOptions loadOptions) {

            var queryParams = Request.GetQueryNameValuePairs().ToDictionary(x => x.Key, x => x.Value);

            int CoolerID; 
                
            bool success = Int32.TryParse(queryParams["CoolerID"], out CoolerID);
            if (!success)
            {
                CoolerID = 0;
            }
       
            var cocresponses = _context.COCResponses.Select(i => new {
                i.ID,
                i.CoolerID,
                i.COCQuestionID,
                i.ResponseValue,
                i.COCQuestionText
            }).Where(x => x.CoolerID == CoolerID);

            
            // If you work with a large amount of data, consider specifying the PaginateViaPrimaryKey and PrimaryKey properties.
            // In this case, keys and data are loaded in separate queries. This can make the SQL execution plan more efficient.
            // Refer to the topic https://github.com/DevExpress/DevExtreme.AspNet.Data/issues/336.
            // loadOptions.PrimaryKey = new[] { "ID" };
            // loadOptions.PaginateViaPrimaryKey = true;

            return Request.CreateResponse(await DataSourceLoader.LoadAsync(cocresponses, loadOptions));
        }

        [HttpPost]
        public async Task<HttpResponseMessage> Post(FormDataCollection form) {
            var model = new COCResponse();
            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            Validate(model);
            if (!ModelState.IsValid)
                return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            var result = _context.COCResponses.Add(model);
            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.Created, new { result.ID });
        }



        [HttpGet]
        public HttpResponseMessage GetQuestionData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(QuestionData.Question, loadOptions));
        }


        public HttpResponseMessage GetCoolingData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(CoolingData.Question, loadOptions));
        }

        public HttpResponseMessage GetPackingData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(PackingData.Question, loadOptions));
        }


        public HttpResponseMessage GetShippingData(DataSourceLoadOptions loadOptions)
        {
            return Request.CreateResponse(DataSourceLoader.Load(ShippingData.Question, loadOptions));
        }

        [HttpPut]
        public async Task<HttpResponseMessage> Put(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.COCResponses.FirstOrDefaultAsync(item => item.ID == key);
            if(model == null)
                return Request.CreateResponse(HttpStatusCode.Conflict, "Object not found");

            var values = JsonConvert.DeserializeObject<IDictionary>(form.Get("values"));
            PopulateModel(model, values);

            //Validate(model);
            //if (!ModelState.IsValid)
              //  return Request.CreateErrorResponse(HttpStatusCode.BadRequest, GetFullErrorMessage(ModelState));

            await _context.SaveChangesAsync();

            return Request.CreateResponse(HttpStatusCode.OK);
        }

        [HttpDelete]
        public async Task Delete(FormDataCollection form) {
            var key = Convert.ToInt32(form.Get("key"));
            var model = await _context.COCResponses.FirstOrDefaultAsync(item => item.ID == key);

            _context.COCResponses.Remove(model);
            await _context.SaveChangesAsync();
        }


        [HttpGet]
        public async Task<HttpResponseMessage> COCQuestionsLookup(DataSourceLoadOptions loadOptions) {
            var lookup = from i in _context.COCQuestions
                         orderby i.Question
                         select new {
                             Value = i.ID,
                             Text = i.Question
                         };
            return Request.CreateResponse(await DataSourceLoader.LoadAsync(lookup, loadOptions));
        }


        private void PopulateModel(COCResponse model, IDictionary values) {
            string ID = nameof(COCResponse.ID);
         ///   string SISID = nameof(COCResponse.SISID);
            string COCQUESTION_ID = nameof(COCResponse.COCQuestionID);
            string RESPONSE_VALUE = nameof(COCResponse.ResponseValue);

            if(values.Contains(ID)) {
                model.ID = Convert.ToInt32(values[ID]);
            }

        
            if(values.Contains(COCQUESTION_ID)) {
                model.COCQuestionID = values[COCQUESTION_ID] != null ? Convert.ToInt32(values[COCQUESTION_ID]) : (int?)null;
            }

            if(values.Contains(RESPONSE_VALUE)) {
                model.ResponseValue = Convert.ToString(values[RESPONSE_VALUE]);
            }
        }

        private string GetFullErrorMessage(ModelStateDictionary modelState) {
            var messages = new List<string>();

            foreach(var entry in modelState) {
                foreach(var error in entry.Value.Errors)
                    messages.Add(error.ErrorMessage);
            }

            return String.Join(" ", messages);
        }

        protected override void Dispose(bool disposing) {
            if (disposing) {
                _context.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}