﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Views
{
    public class COCResponsesController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: COCResponses
        public ActionResult Index(int ID)
        {
            ViewBag.ID = ID;
            return View();
        }


        [HttpPost]
        public ActionResult CloneCooler(int id)
        {
            //insert locations 
            var Util = new Utilities();
            var created = Util.CloneCooler(id);
            //redirect on jquery on client side .post does not play well with  redirecttoaction
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        [HttpPost]
        public ActionResult POST()
        {
            var SISID = Request.Form["SISID"];
            var CoolerID = Request.Form["CoolerID"];
            try
            {
                
                var PMName = Request.Form[2];
                var PMDate = Request.Form[3];
               
                if (!String.IsNullOrEmpty(PMName))
                {
                    var res = new PMNotification();
                    res.CoolerID = Int32.Parse(CoolerID);
                    res.PMName = PMName;
                    if (!String.IsNullOrEmpty(PMDate))
                    {
                        var parsedDate = DateTime.Parse(PMDate);
                        res.PMTime = parsedDate;
                    }
                  
                    db.PMNotifications.Add(res);
                    db.SaveChanges();
                }

                 return RedirectToAction("Index/" + SISID, "COCResponses");
            }

            catch (Exception)
            {
                ModelState.AddModelError("name", "Please enter in a correct time and PMName");

                
                return RedirectToAction("Index/"+ SISID, "COCResponses");
            }          
        }

        // GET: COCResponses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COCResponse cOCResponse = db.COCResponses.Find(id);
            if (cOCResponse == null)
            {
                return HttpNotFound();
            }
            return View(cOCResponse);
        }

        // GET: COCResponses/Create
        public ActionResult Create()
        {
            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question");
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder");
            return View();
        }

        // POST: COCResponses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SISID,COCQuestionID,Response")] COCResponse cOCResponse)
        {
            if (ModelState.IsValid)
            {
                db.COCResponses.Add(cOCResponse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question", cOCResponse.COCQuestionID);
            //ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", cOCResponse.SISID);
            return View(cOCResponse);
        }

        // GET: COCResponses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COCResponse cOCResponse = db.COCResponses.Find(id);
            if (cOCResponse == null)
            {
                return HttpNotFound();
            }
            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question", cOCResponse.COCQuestionID);
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", cOCResponse.ID);
            return View(cOCResponse);
        }

        // POST: COCResponses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SISID,COCQuestionID,Response")] COCResponse cOCResponse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cOCResponse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question", cOCResponse.COCQuestionID);
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", cOCResponse.ID);
            return View(cOCResponse);
        }


        [HttpPost]
        public ActionResult CheckValidation(int id)
        {

            try
            {
                var result = db.COCResponses.Where(m => m.Cooler.SISID == id).ToList();
                if (result.Count() == 0)
                {
                    return Json(new { error = "Please enter a cooler", ID = 1 });
                }
                foreach (var response in result)
                {
                    Console.WriteLine(response.ResponseValue);

                    if (response.ResponseValue == "" || response.ResponseValue is null)
                    {
                        return Json(new { error = "Please answer all COC Questions", ID = 1 });
                    }
                    if (response.COCQuestionID == 12 && response.ResponseValue.ToUpper() == "YES")
                    {

                        var pmresponse = db.PMNotifications.Where(m => m.CoolerID == response.CoolerID).ToList();
                        if (pmresponse.Count() == 0)
                        {
                            return Json(new { error = "Please Enter PM information if PM was notified of discrepancies", ID = 1 });
                        }
                    }
                }
                return Json(new { error = "OK", ID = 2 });
            }
            catch (Exception e)
            {

                return Json(new { error = e.InnerException.ToString(), ID = 1 });
            }
            //insert locations 
            
        }

        // GET: COCResponses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COCResponse cOCResponse = db.COCResponses.Find(id);
            if (cOCResponse == null)
            {
                return HttpNotFound();
            }
            return View(cOCResponse);
        }

        // POST: COCResponses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            COCResponse cOCResponse = db.COCResponses.Find(id);
            db.COCResponses.Remove(cOCResponse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
