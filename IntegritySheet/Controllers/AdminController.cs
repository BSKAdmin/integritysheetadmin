using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Views
{
    public class AdminController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: COCResponses
        public ActionResult Index()
        {
            return View();
        }


        [HttpPost]
        public ActionResult CloneCooler(int id)
        {
            //insert locations 
            var Util = new Utilities();
            var created = Util.CloneCooler(id);
            //redirect on jquery on client side .post does not play well with  redirecttoaction
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }
        [HttpPost]
        public ActionResult POST()
        {
            var SISID = Request.Form["SISID"];
            var CoolerID = Request.Form["CoolerID"];
            try
            {

                var PMName = Request.Form[2];
                var PMDate = Request.Form[3];

                if (!String.IsNullOrEmpty(PMName))
                {
                    var res = new PMNotification();
                    res.CoolerID = Int32.Parse(CoolerID);
                    res.PMName = PMName;
                    if (!String.IsNullOrEmpty(PMDate))
                    {
                        var parsedDate = DateTime.Parse(PMDate);
                        res.PMTime = parsedDate;
                    }

                    db.PMNotifications.Add(res);
                    db.SaveChanges();
                }

                return RedirectToAction("Index/" + SISID, "COCResponses");
            }

            catch (Exception)
            {
                ModelState.AddModelError("name", "Please enter in a correct time and PMName");


                return RedirectToAction("Index/" + SISID, "COCResponses");
            }
        }



        [HttpPost]
        public ActionResult ResetIS(int id)
        {
            using (var _context = new IntegritySheetEntities())
            {
                var result = db.SampleIntegritySheets.SingleOrDefault(b => b.ID == id);
                if (result != null)
                {
                    try
                    {
                        result.StatusID = 1;
                        db.SaveChanges();
                    }
                    catch (Exception ex)
                    {
                        throw;
                    }
                }
            }
            return new HttpStatusCodeResult(HttpStatusCode.OK);
        }



        // GET: COCResponses/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COCResponse cOCResponse = db.COCResponses.Find(id);
            if (cOCResponse == null)
            {
                return HttpNotFound();
            }
            return View(cOCResponse);
        }

        // GET: COCResponses/Create
        public ActionResult Create()
        {
            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question");
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder");
            return View();
        }

        // POST: COCResponses/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SISID,COCQuestionID,Response")] COCResponse cOCResponse)
        {
            if (ModelState.IsValid)
            {
                db.COCResponses.Add(cOCResponse);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question", cOCResponse.COCQuestionID);
            //ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", cOCResponse.SISID);
            return View(cOCResponse);
        }

        // GET: COCResponses/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COCResponse cOCResponse = db.COCResponses.Find(id);
            if (cOCResponse == null)
            {
                return HttpNotFound();
            }
            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question", cOCResponse.COCQuestionID);
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", cOCResponse.ID);
            return View(cOCResponse);
        }

        // POST: COCResponses/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SISID,COCQuestionID,Response")] COCResponse cOCResponse)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cOCResponse).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.COCQuestionID = new SelectList(db.COCQuestions, "ID", "Question", cOCResponse.COCQuestionID);
            ViewBag.SISID = new SelectList(db.SampleIntegritySheets, "ID", "WorkOrder", cOCResponse.ID);
            return View(cOCResponse);
        }

        // GET: COCResponses/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COCResponse cOCResponse = db.COCResponses.Find(id);
            if (cOCResponse == null)
            {
                return HttpNotFound();
            }
            return View(cOCResponse);
        }

        // POST: COCResponses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            COCResponse cOCResponse = db.COCResponses.Find(id);
            db.COCResponses.Remove(cOCResponse);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
