
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Services;

namespace IntegritySheet.Controllers
{
    [Authorize(Users = "BSK\\gcushing,BSK\\kmorton,BSK\\mlear,BSK\\vhernandez,BSK\\analyst")]

    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult SignOutCallback()
        {
            return View();
        }




    }
}