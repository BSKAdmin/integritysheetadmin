﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using IntegritySheet.Models;

namespace IntegritySheet.Views
{
    public class SampleLocationContainersController : Controller
    {
        private IntegritySheetEntities db = new IntegritySheetEntities();

        // GET: SampleLocationContainers

        public ActionResult Index(int ID, int RETURNID)
        {
            ViewBag.ID = ID;
            ViewBag.RETURNID = RETURNID;
            SampleLocation sp = db.SampleLocations.First(m => m.ID == ID);
            
            ViewBag.Location = sp.Description;
            return View();
        }

        // GET: SampleLocationContainers/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocationContainer sampleLocationContainer = db.SampleLocationContainers.Find(id);
            if (sampleLocationContainer == null)
            {
                return HttpNotFound();
            }
            return View(sampleLocationContainer);
        }

        // GET: SampleLocationContainers/Create
        public ActionResult Create()
        {
            ViewBag.BottleID = new SelectList(db.Containers, "ID", "Bottle");
            ViewBag.SLID = new SelectList(db.SampleLocations, "ID", "Description");
            return View();
        }

        // POST: SampleLocationContainers/Create
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SLID,BottleID,Response,SplitorPreserved")] SampleLocationContainer sampleLocationContainer)
        {
            if (ModelState.IsValid)
            {
                db.SampleLocationContainers.Add(sampleLocationContainer);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.BottleID = new SelectList(db.Containers, "ID", "Bottle", sampleLocationContainer.BottleID);
            ViewBag.SLID = new SelectList(db.SampleLocations, "ID", "Description", sampleLocationContainer.SLID);
            return View(sampleLocationContainer);
        }

        // GET: SampleLocationContainers/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocationContainer sampleLocationContainer = db.SampleLocationContainers.Find(id);
            if (sampleLocationContainer == null)
            {
                return HttpNotFound();
            }
            ViewBag.BottleID = new SelectList(db.Containers, "ID", "Bottle", sampleLocationContainer.BottleID);
            ViewBag.SLID = new SelectList(db.SampleLocations, "ID", "Description", sampleLocationContainer.SLID);
            return View(sampleLocationContainer);
        }

        // POST: SampleLocationContainers/Edit/5
        // To protect from overposting attacks, enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SLID,BottleID,Response,SplitorPreserved")] SampleLocationContainer sampleLocationContainer)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sampleLocationContainer).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BottleID = new SelectList(db.Containers, "ID", "Bottle", sampleLocationContainer.BottleID);
            ViewBag.SLID = new SelectList(db.SampleLocations, "ID", "Description", sampleLocationContainer.SLID);
            return View(sampleLocationContainer);
        }

        // GET: SampleLocationContainers/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SampleLocationContainer sampleLocationContainer = db.SampleLocationContainers.Find(id);
            if (sampleLocationContainer == null)
            {
                return HttpNotFound();
            }
            return View(sampleLocationContainer);
        }

        // POST: SampleLocationContainers/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SampleLocationContainer sampleLocationContainer = db.SampleLocationContainers.Find(id);
            db.SampleLocationContainers.Remove(sampleLocationContainer);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
