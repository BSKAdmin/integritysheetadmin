//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPlan
    {
        public long Seq { get; set; }
        public long PlanHashID { get; set; }
        public Nullable<int> DBID { get; set; }
        public Nullable<long> BatchSeq { get; set; }
        public Nullable<long> StmtSeq { get; set; }
        public int SPID { get; set; }
        public System.DateTime StartTime { get; set; }
        public Nullable<byte> DOP { get; set; }
    }
}
