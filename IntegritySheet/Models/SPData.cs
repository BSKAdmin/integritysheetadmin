﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class SPData
    {
        public static List<SPDataModel> SP = new List<SPDataModel> {
            new SPDataModel {
                ID = "",
                Name = ""
            },
            new SPDataModel {
                ID = "S",
                Name = "Split"
            },
            new SPDataModel {
                ID = "P",
                Name = "Preserved"
            },
            new SPDataModel {
                ID = "X",
                Name = "Split And Preserved"
            },
        };
    }
}