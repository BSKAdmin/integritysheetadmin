﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace IntegritySheet.Models
{
    public class COCResponseValidationModel
    {
        public int ID { get; set; }
        public Nullable<int> CoolerID { get; set; }
        public Nullable<int> COCQuestionID { get; set; }
        [Required]
        public string ResponseValue { get; set; }
        public string COCQuestionText { get; set; }

    }
}