//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class PROPERTy
    {
        public string Laboratory { get; set; }
        public string Designator { get; set; }
        public string Abbreviation { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Phone { get; set; }
        public string Fax { get; set; }
        public string DomainName { get; set; }
        public string LicenseCode { get; set; }
        public Nullable<bool> BusinessDay { get; set; }
        public Nullable<bool> RequireMarketInfo { get; set; }
        public Nullable<bool> InvoiceOption { get; set; }
        public Nullable<bool> HTOption { get; set; }
        public Nullable<bool> BillTrips { get; set; }
        public Nullable<bool> IntegerQC { get; set; }
        public Nullable<System.DateTime> EndOfDay { get; set; }
        public string DateFormat { get; set; }
        public Nullable<short> StandardExpires { get; set; }
        public Nullable<short> SolidFigs { get; set; }
        public string SolidMethod { get; set; }
        public Nullable<short> SolidThreshold { get; set; }
        public Nullable<short> PrepThreshold { get; set; }
        public Nullable<float> AirFactor { get; set; }
        public Nullable<short> StandardFigs { get; set; }
        public Nullable<short> TimeOut { get; set; }
        public Nullable<short> TimeZone { get; set; }
        public Nullable<short> DefaultTAT { get; set; }
        public Nullable<System.DateTime> DBStartDate { get; set; }
        public Nullable<short> DispDays { get; set; }
        public Nullable<float> HazLevel { get; set; }
        public Nullable<short> BidDays { get; set; }
        public string D_String { get; set; }
        public string ND_String { get; set; }
        public string Initial { get; set; }
        public string Available { get; set; }
        public string Batched { get; set; }
        public string Leached { get; set; }
        public string Prepared { get; set; }
        public string Analyzed { get; set; }
        public string Reviewed { get; set; }
        public string Reported { get; set; }
        public string Invoiced { get; set; }
        public string Final { get; set; }
        public string Cancelled { get; set; }
        public string Subcontracted { get; set; }
        public string Hold { get; set; }
        public string WrkCode { get; set; }
        public string SampleCode { get; set; }
        public string StandardCode { get; set; }
        public string BatchCode { get; set; }
        public string InvoiceCode { get; set; }
        public string SequenceCode { get; set; }
        public string CalibrationCode { get; set; }
        public string DefaultRpt { get; set; }
        public string CLabel1 { get; set; }
        public string CLabel2 { get; set; }
        public string CLabel3 { get; set; }
        public string CLabel4 { get; set; }
        public string CLabel5 { get; set; }
        public string ALabel1 { get; set; }
        public string ALabel2 { get; set; }
        public string ALabel3 { get; set; }
        public string ALabel4 { get; set; }
        public string ALabel5 { get; set; }
        public string ElmVersion { get; set; }
        public Nullable<System.DateTime> ElmVersionDate { get; set; }
        public byte[] Logo { get; set; }
        public string FacilityCode { get; set; }
        public string CleanupCode { get; set; }
        public string DocManLicenseCode { get; set; }
        public string RepDBType { get; set; }
        public string RepDBPath { get; set; }
        public string RepDBName { get; set; }
        public string RepDBID { get; set; }
        public string RepDBPW { get; set; }
        public string CRMLicenseCode { get; set; }
        public string CRMDBPath { get; set; }
        public string CRMDBID { get; set; }
        public string CRMDBPW { get; set; }
        public string CRMClientViewName { get; set; }
        public Nullable<System.DateTime> Exported { get; set; }
        public Nullable<System.DateTime> Imported { get; set; }
        public string MobileDB { get; set; }
        public bool IsMobileDB { get; set; }
        public string Approved { get; set; }
        public Nullable<short> HstDBType { get; set; }
        public Nullable<short> HstDBJetType { get; set; }
        public string HstDBServerName { get; set; }
        public string HstDBServerPath { get; set; }
        public string HstDBDatabaseName { get; set; }
        public string HstDBSRXFileName { get; set; }
        public Nullable<short> ExtDBType { get; set; }
        public Nullable<short> ExtDBJetType { get; set; }
        public string ExtDBServerName { get; set; }
        public string ExtDBServerPath { get; set; }
        public string ExtDBDatabaseName { get; set; }
        public string ExtDBSRXFileName { get; set; }
        public Nullable<short> RemoteAppDBType { get; set; }
        public Nullable<short> RemoteAppDBJetType { get; set; }
        public string RemoteAppDBServerName { get; set; }
        public string RemoteAppDBServerPath { get; set; }
        public string RemoteAppDBDatabaseName { get; set; }
        public string RemoteAppDBSRXFileName { get; set; }
        public Nullable<System.DateTime> DBEndDate { get; set; }
        public string LabLD { get; set; }
        public string DefaultInv { get; set; }
        public string BottleCode { get; set; }
        public string DefLocation { get; set; }
        public string InvoicePrefix { get; set; }
        public string Reviewable { get; set; }
        public string Reportable { get; set; }
        public string Pending { get; set; }
        public string ECHSRX { get; set; }
        public string MTRSRX { get; set; }
        public string CCBSRX { get; set; }
    }
}
