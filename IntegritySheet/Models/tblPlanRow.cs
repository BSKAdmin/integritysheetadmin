//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class tblPlanRow
    {
        public long Seq { get; set; }
        public Nullable<long> Rows { get; set; }
        public Nullable<long> Executes { get; set; }
        public Nullable<double> EstimateRows { get; set; }
        public Nullable<double> EstimateExecutes { get; set; }
        public short RowOrder { get; set; }
    }
}
