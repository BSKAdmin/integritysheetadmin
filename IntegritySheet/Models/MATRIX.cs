//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class MATRIX
    {
        public string Matrix1 { get; set; }
        public bool Solids { get; set; }
        public bool isVirtual { get; set; }
        public string UserGUID { get; set; }
        public string UserGUIDDesc { get; set; }
        public string Elm7HostKey { get; set; }
    }
}
