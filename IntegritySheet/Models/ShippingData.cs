﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class ShippingData
    {
            public static List<ShippingDataModel> Question = new List<ShippingDataModel> {
            new ShippingDataModel {
                ID = 1,
                Name = "ONTRAC"
            },
            new ShippingDataModel {
                ID = 2,
                Name = "UPS"
            },
            new ShippingDataModel {
                ID = 3,
                Name = "GSO"
            },
            new ShippingDataModel {
                ID = 4,
                Name = "WALK-IN"
            },

            new ShippingDataModel {
                ID = 5,
                Name = "FEDEX"
            },
            new ShippingDataModel {
                ID = 6,
                Name = "Other"
            }
        };
        
    }
}
