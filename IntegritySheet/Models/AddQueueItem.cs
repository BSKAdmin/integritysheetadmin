﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class AddQueueItem 
    {
        public Itemdata itemData { get; set; }
    }

    public class Itemdata
    {
        public string Name { get; set; }
        public object SpecificContent { get; set; }
        public string Reference { get; set; }
    }

    public class Specificcontent2
    {
        public string Id { get; set; }
        public string SequenceNo { get; set; }

        public string SequenceTest { get; set; }
        
    }

}