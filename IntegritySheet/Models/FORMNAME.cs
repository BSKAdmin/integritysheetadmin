//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace IntegritySheet.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class FORMNAME
    {
        public string FormName1 { get; set; }
        public string FormType { get; set; }
        public string RptName { get; set; }
        public string PDFFilePattern { get; set; }
        public bool isTOC { get; set; }
    }
}
