﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IntegritySheet.Models
{
    public class StatusData
    {
            public static List<StatusDataModel> Question = new List<StatusDataModel> {
            new StatusDataModel {
                ID = 1,
                Name = "Not Sent To Robot"
            },
            new StatusDataModel {
                ID = 3,
                Name = "Sent To Robot"
            }
        };
        
    }
}
