﻿using System;
using System.Collections.Generic;
using System.DirectoryServices.AccountManagement;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.Graph;
using Microsoft.Graph.Auth;
using Microsoft.Identity.Client;

using System.Text;
using System.Threading.Tasks;



namespace IntegritySheet
{
    public class AuthorizeADAttribute : AuthorizeAttribute
    {
        public string Groups { get; set; }

        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            if (base.AuthorizeCore(httpContext))
            {
                /* Return true immediately if the authorization is not 
                locked down to any particular AD group */
                if (String.IsNullOrEmpty(Groups))
                    return true;

                // Get the AD groups
                var groups = Groups.Split(',').ToList<string>();
                var User = System.Web.HttpContext.Current.User;


                var user2 = User.Identity.Name;
                string currUser = System.Security.Principal.WindowsIdentity.GetCurrent().Name.ToString();
                HttpContext currentContext = HttpContext.Current;
                string userName = currentContext.User.Identity.Name;
                // Verify that the user is in the given AD group (if any)
                var context = new PrincipalContext(ContextType.Domain, "fal-dc.bsk.local");
                var userPrincipal = UserPrincipal.FindByIdentity(context,
                                                     IdentityType.SamAccountName,
                                                     currUser);

                foreach (var group in groups)
                    if (userPrincipal.IsMemberOf(context, IdentityType.Name, group))
                        return true;
            }
            return false;
        }
    }
}